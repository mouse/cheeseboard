#!/usr/bin/env bb

(require '[babashka.fs :as fs])

(if (not (fs/exists? "glyphs")) (fs/create-dir "glyphs") ())
(vec
  (map-indexed
    (fn [index path]
          (fs/create-link (format "glyphs/%X.svg" (+ 9812 index)) path))
    (flatten (map (fn [[dir colour]]
                      (map (fn [piece]
                             (str dir "/" (fs/file-name dir)
                                      "-" piece
                                      "-" colour ".svg"))
                        ["king" "queen" "rook" "bishop" "knight" "pawn"]))
             [["art/sprites/mouse", "white"], ["art/sprites/cat", "black"]]))))
