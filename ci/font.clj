#!/usr/bin/env bb

(require '[clojure.core.match :as m])
(require '[babashka.process :refer [shell]])
(require '[clojure.string :as str])
(require '[babashka.fs :as fs])

(let
  [versions (m/match
       (str (-> (shell {:continue true :out :string :err nil}
                 "git" "describe" "--tags" "--abbrev=0") :out str))
       "" [0 0]
       version (str/split (subs (str/trim version) 1) #"\."))
   name (str/replace
       (last
         (str/split
           (str/trim
             (str (-> (shell {:continue true :out :string}
                      "git" "config" "--local" "remote.origin.url") :out str)))
         #"/"))
     #"\.git" "")
   glyphs (vec (map str (fs/match "glyphs" "regex:.*.svg")))]

  (vec (map (fn [color_format] (shell
    (str/join \space (flatten
      ["nanoemoji" "--build_dir" "fonts"
                   "--output_file" (format "%s-%s.ttf" name color_format)
                   "--family" (str/capitalize name)
                   "--version_major" (first versions)
                   "--version_minor" (last versions)
                   "--color_format" color_format
                   glyphs]))))
    ["glyf" "glyf_colr_0" "glyf_colr_1"
     "cff_colr_0" "cff_colr_1" "cff2_colr_0" "cff2_colr_1"
     "picosvg" "picosvgz"
     "untouchedsvg" "untouchedsvgz"
     "cbdt" "sbix"])))


