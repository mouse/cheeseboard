#!/usr/bin/env bb

(require '[babashka.process :refer [shell]])
(require '[babashka.fs :as fs])

(if (not (fs/exists? "glyphs")) (fs/create-dir "glyphs") ())
(vec
  (map-indexed
    (fn [index path]
          (fs/create-link (format "glyphs/%X.svg" (+ 983040 index)) path))
    (flatten (map (fn [dir]
                    (map (fn [colour]
                           (map (fn [piece]
                                  (str dir "/" (fs/file-name dir)
                                           "-" piece
                                           "-" colour ".svg"))
                             ["king" "queen" "rook" "bishop" "knight" "pawn"]))
                         ["white", "black"]))
             (sort-by (fn [dir]
                        (str (-> (shell {:out :string}
                          "git" "log" "--follow"
                                      "--max-count=1"
                                      "--format=%ct"
                                      "--date=default" dir) :out str)))
                      (map str (fs/list-dir "art/sprites")))))))
