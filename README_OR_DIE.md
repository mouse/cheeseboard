# Cheeseboard
## Dev
### Code
Build and run the rust project:
```shell
wasm-pack build --no-typescript --target no-modules --dev
python -m http.server
```

#### With Nix Flake
Build and run the rust project:
```shell
nix build
cd result
python -m http.server
```

#### Live Coding
Watching and building the rust project:
```shell
cargo watch -- wasm-pack build --no-typescript --target no-modules --dev
python -m http.server
```

##### With [TabFS](https://omar.website/tabfs)
Same with page reloading:
```shell
export TAB=$(find /run/user/1000/tabfs/tabs/by-title/ -name "Cheeseboard*" -print -quit)/control
cargo watch -- sh -c "wasm-pack build --no-typescript --target no-modules --dev && echo reload > \${TAB}"
```

### Test
Run the tests:
```shell
WASM_BINDGEN_TEST_TIMEOUT=60 wasm-pack test --firefox --headless
```

### Doc
Build and Run:
```shell
lualatex --shell-escape --interaction errorstopmode --jobname "map" \
  "\RequirePackage[extension=.pdf]{texdepends}\input{doc/map.tex}"
zathura map.pdf
```

## Link
Some helpfull links:
* [game](https://mouse.frama.io/cheeseboard)
* [map](https://mouse.frama.io/cheeseboard/map.pdf)

* [cheeseboard-glyf](https://mouse.frama.io/cheeseboard/cheeseboard-glyf.ttf)
* [cheeseboard-glyf-colr-0](https://mouse.frama.io/cheeseboard/cheeseboard-glyf_colr_0.ttf)
* [cheeseboard-glyf-colr-1](https://mouse.frama.io/cheeseboard/cheeseboard-glyf_colr_1.ttf)
* [cheeseboard-cff-colr-0](https://mouse.frama.io/cheeseboard/cheeseboard-cff_colr_0.ttf)
* [cheeseboard-cff-colr-1](https://mouse.frama.io/cheeseboard/cheeseboard-cff_colr_1.ttf)
* [cheeseboard-cff2-colr-0](https://mouse.frama.io/cheeseboard/cheeseboard-cff2_colr_0.ttf)
* [cheeseboard-cff2-colr-1](https://mouse.frama.io/cheeseboard/cheeseboard-cff2_colr_1.ttf)
* [cheeseboard-picosvg](https://mouse.frama.io/cheeseboard/cheeseboard-picosvg.ttf)
* [cheeseboard-picosvgz](https://mouse.frama.io/cheeseboard/cheeseboard-picosvgz.ttf)
* [cheeseboard-untouchedsvg](https://mouse.frama.io/cheeseboard/cheeseboard-untouchedsvg.ttf)
* [cheeseboard-untouchedsvgz](https://mouse.frama.io/cheeseboard/cheeseboard-untouchedsvgz.ttf)
* [cheeseboard-cbdt](https://mouse.frama.io/cheeseboard/cheeseboard-cbdt.ttf)
* [cheeseboard-sbix](https://mouse.frama.io/cheeseboard/cheeseboard-sbix.ttf)
