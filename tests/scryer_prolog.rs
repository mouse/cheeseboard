use wasm_bindgen_test::*;
use wasm_bindgen_test::wasm_bindgen_test_configure;

use scryer_prolog::machine::Machine;
use scryer_prolog::machine::parsed_results::{QueryResolution, QueryMatch};
use scryer_prolog::machine::parsed_results::Value;

use num_traits::cast::ToPrimitive;

#[cfg(test)]
mod tests {
  use super::*;
  wasm_bindgen_test_configure!(run_in_dedicated_worker);

  static CODE: &str =
    include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/src/", "chess.pl"));

  fn get_marks(
    machine: &mut Machine,
    chessboard: &str,
    selected: &str
  ) -> Vec<String> {
    if let Ok(QueryResolution::Matches(query_matches)) =
      machine.run_query(format!("get_marks(\"{}\", {}, Result).",
      chessboard, selected
    )) {
      if let Some((_, Value::List(values))) =
        query_matches
          .first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
        return values.iter().filter_map(|value: &Value| match value {
          Value::Float(ref value) =>
            value.to_usize().and_then(|value| Some(vec![value.to_string()])),
          Value::List(ref values) =>
            Some(values.iter().filter_map(|value: &Value| match value {
              Value::Float(ref value) =>
                value.to_usize()
                     .and_then(|value| Some(value.to_string())),
              _ => None
            }).collect::<Vec<String>>()),
          _ => None,
        }).flatten().collect::<Vec<String>>()
      }
    }
    unimplemented!()
  }

  fn get_tower_castling(
    machine: &mut Machine,
    chessboard: &str,
    previous_selected: &str,
    selected: &str
  ) -> Option<(String, String)> {
    match machine.run_query(
      format!("get_tower_castling(\"{}\", {}, {}, Result).",
      chessboard, previous_selected, selected
    )) {
      Ok(QueryResolution::Matches(query_matches)) => {
        match query_matches.first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
          Some((_, Value::List(values))) => {
            let mut list = values.iter();
            if let [Some(Value::Float(value_from)),
                    Some(Value::Float(value_to))] = [list.next(), list.next()] {
              return Some((value_from.to_string(), value_to.to_string()))
            }
          },
          _ => {},
        }
      },
      Ok(_) => return None,
      _ => {},
    }
    unimplemented!()
  }

  fn get_pawn_passant(
    machine: &mut Machine,
    chessboard: &str,
    previous_selected: &str,
    selected: &str
  ) -> Option<String> {
    match machine.run_query(
      format!("get_pawn_passant(\"{}\", {}, {}, Result).",
      chessboard, previous_selected, selected
    )) {
      Ok(QueryResolution::Matches(query_matches)) => {
        match query_matches.first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
          Some((_, Value::Float(value))) => {
            return Some(value.to_string())
          },
          _ => {},
        }
      },
      Ok(_) => return None,
      _ => {},
    }
    unimplemented!()
  }

  fn get_promotion(
    machine: &mut Machine,
    chessboard: &str,
    selected: &str
  ) -> Option<String> {
    match machine.run_query(
      format!("get_promotion(\"{}\", {}, Result).",
      chessboard, selected
    )) {
      Ok(QueryResolution::Matches(query_matches)) => {
        match query_matches.first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
          Some((_, Value::String(value))) => {
            return Some(value.to_string())
          },
          _ => {},
        }
      },
      Ok(_) => return None,
      _ => {},
    }
    unimplemented!()
  }

  #[wasm_bindgen_test]
  fn pawn_white_mouvement_double() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "49";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("41"), String::from("33")]);
  }

  #[wasm_bindgen_test]
  fn pawn_black_mouvement_double() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "16";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("24"), String::from("32")]);
  }

  #[wasm_bindgen_test]
  fn pawn_white_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♙', '♞', '♜',
      '♟', '.', '.', '♟', '.', '♟', '♟', '♟',
      '.', '♟', '.', '.', '.', '.', '.', '.',
      '♙', '.', '♟', '.', '♟', '.', '.', '.',
      '.', '.', '.', '♙', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♟', '♙', '♙', '.', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "36";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("28"), String::from("29"),
                            String::from("27")]);

    let selected = "25";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("17"), String::from("18")]);

    let selected = "56";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("48"), String::from("40")]);
  }

  #[wasm_bindgen_test]
  fn pawn_white_capture_passant() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '.', '♟', '.', '♟', '.',
      '.', '.', '.', '♟', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '♟',
      '♙', '.', '.', '♟', '♙', '♟', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '♙', '♙', '♙', '.', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "33";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("25")]);

    let selected = "37";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("29")]);

    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '.', '♟', '.', '♟', '.',
      '.', '.', '.', '♟', '.', '.', '.', '.',
      '♟', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '♟', '♙', '.', '.', '♙',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '♙', '♙', '♙', '.', '♙', '♙', '.',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "37";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("29")]);

    let selected = "40";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("32")]);
  }

  #[wasm_bindgen_test]
  fn pawn_black_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♙', '♞', '♜',
      '.', '.', '.', '♟', '.', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♟', '.', '♟', '.', '.', '.', '.', '.',
      '.', '♙', '.', '♙', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '♙', '♙', '.', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "25";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("33"), String::from("34")]);

    let selected = "27";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("35"), String::from("34"),
                            String::from("36")]);
  }

  #[wasm_bindgen_test]
  fn pawn_black_capture_passant() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '.', '♟', '.', '♟', '.',
      '.', '.', '♙', '♟', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♟', '.', '.', '.', '.', '.', '.', '♙',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '♙', '♙', '♙', '.', '♙', '♙', '.',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "20";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("28")]);

    let selected = "33";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("41")]);

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '.', '♟', '.', '♟', '.',
      '.', '.', '.', '♟', '♙', '.', '.', '.',
      '♟', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '♙', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '♙', '♙', '♙', '.', '♙', '♙', '.',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "20";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("28")]);

    let selected = "40";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("48"), String::from("47")]);

    // Pawn Enemy
    let selected = "40";
    let result = get_pawn_passant(&mut machine, &chessboard, &selected, &"47");
    assert_eq!(result, Some(String::from("39")));
  }

  #[wasm_bindgen_test]
  fn rook_white_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '♙', '.', '.', '♙', '.', '.', '.', '.',
      '♖', '♝', '♙', '♖', '♙', '♙', '♙', '♖',
      '♙', '.', '.', '♟', '.', '♖', '♙', '♙',
      '.', '.', '.', '.', '♙', '♙', '♙', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '.',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '.'
    );
    let selected = "32";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("16"), String::from("24")]);

    let selected = "28";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("36")]);

    let selected = "25";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("26")]);

    let selected = "38";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("36"), String::from("37")]);
  }

  #[wasm_bindgen_test]
  fn rook_black_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '♙', '.', '.', '.', '.', '.',
      '.', '♙', '♜', '♘', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "43";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("35"), String::from("51"),
                            String::from("44"), String::from("42")]);
  }

  #[wasm_bindgen_test]
  fn knight_white_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '♟', '.', '.', '.', '.', '.',
      '.', '♙', '.', '.', '.', '♛', '.', '.',
      '.', '.', '.', '♘', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "44";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("29"), String::from("27"), String::from("38")]);
  }

  #[wasm_bindgen_test]
  fn knight_black_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '♟', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♞', '.', '♟', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "41";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("51"), String::from("58"),
                            String::from("35")]);
  }

  #[wasm_bindgen_test]
  fn bishop_white_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '♟', '.', '♟', '.', '.', '.',
      '.', '.', '.', '♗', '.', '.', '.', '.',
      '.', '.', '♟', '.', '♟', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "28";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("19"), String::from("21"),
                            String::from("35"), String::from("37")]);
  }

  #[wasm_bindgen_test]
  fn bishop_black_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '♙', '.', '♙', '.', '.', '.',
      '.', '.', '.', '♝', '.', '.', '.', '.',
      '.', '.', '♙', '.', '♙', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "28";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("19"), String::from("21"),
                            String::from("35"), String::from("37")]);
  }

  #[wasm_bindgen_test]
  fn queen_white_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '♟', '♟', '♟', '.', '.', '.',
      '.', '.', '♟', '♕', '♟', '.', '.', '.',
      '.', '.', '♟', '♟', '♟', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "28";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("20"), String::from("19"),
                            String::from("21"), String::from("36"),
                            String::from("35"), String::from("37"),
                            String::from("29"), String::from("27")]);
  }

  #[wasm_bindgen_test]
  fn queen_black_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '♙', '♙', '♙', '.', '♟', '♟',
      '.', '.', '♙', '♛', '♙', '.', '♛', '.',
      '.', '.', '♙', '♙', '♙', '♙', '.', '.',
      '.', '.', '.', '.', '.', '♙', '.', '♙',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "28";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("20"), String::from("19"),
                            String::from("21"), String::from("36"),
                            String::from("35"), String::from("37"),
                            String::from("29"), String::from("27")]);

    let selected = "31";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("22"), String::from("55"),
                            String::from("47"), String::from("39"),
                            String::from("38"), String::from("40"), 
                            String::from("32"), String::from("29"),
                            String::from("30")]);
  }

  #[wasm_bindgen_test]
  fn king_white_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '♟', '♟', '♟', '.', '.', '.',
      '.', '.', '♟', '♔', '♟', '.', '.', '.',
      '.', '.', '♟', '♟', '♟', '.', '.', '.',
      '.', '.', '.', '.', '.', '♙', '.', '♙',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "28";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("20"), String::from("27"),
                            String::from("19"), String::from("21"),
                            String::from("29"), String::from("36"),
                            String::from("35"), String::from("37")]);
  }

  #[wasm_bindgen_test]
  fn king_black_capture() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '♙', '♙', '♙', '.', '.', '.',
      '.', '.', '♙', '♚', '♙', '.', '.', '.',
      '.', '.', '♙', '♙', '♙', '.', '.', '.',
      '.', '.', '.', '.', '.', '♙', '.', '♙',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "28";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("20"), String::from("27"),
                            String::from("19"), String::from("21"),
                            String::from("29"), String::from("36"),
                            String::from("35"), String::from("37")]);
  }

  #[wasm_bindgen_test]
  fn king_white_castling() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '.', '.', '.', '♔', '.', '.', '♖'
    );
    let selected = "61";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("60"), String::from("62"),
                            String::from("63"), String::from("59")]);

    // Tower
    let result = get_tower_castling(&mut machine, &chessboard, &selected, &"63");

    assert_eq!(result, Some((String::from("64"), String::from("62"))));

    let result = get_tower_castling(&mut machine, &chessboard, &selected, &"59");

    assert_eq!(result, Some((String::from("57"), String::from("60"))));
  }

  #[wasm_bindgen_test]
  fn king_black_castling() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '.', '.', '.', '♚', '.', '.', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "5";
    let result: Vec<String> = get_marks(&mut machine, &chessboard, &selected);

    assert_eq!(result, vec![String::from("4"), String::from("6"),
                            String::from("7"), String::from("3")]);

    // Tower
    let result = get_tower_castling(&mut machine, &chessboard, &selected, &"7");

    assert_eq!(result, Some((String::from("8"), String::from("6"))));

    let result = get_tower_castling(&mut machine, &chessboard, &selected, &"3");

    assert_eq!(result, Some((String::from("1"), String::from("4"))));
  }

  #[wasm_bindgen_test]
  fn pawn_white_promotion() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '.', '.', '.', '♛', '♚', '♝', '♞', '♜',
      '♙', '.', '.', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♙', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "9";
    let result = get_promotion(&mut machine, &chessboard, &selected);

    assert_eq!(result, Some(String::from("♙♕♗♘♖")));

    let selected = "49";
    let result = get_promotion(&mut machine, &chessboard, &selected);

    assert!(result.is_none());
  }

  #[wasm_bindgen_test]
  fn pawn_black_promotion() {
    let mut machine = Machine::new_lib();
    machine.load_module_string("facts", CODE.to_string());

    let chessboard = concat!(
      '♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜',
      '♟', '♟', '♟', '♟', '♟', '♟', '♟', '♟',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '.', '.', '.', '.', '.', '.', '.', '.',
      '♟', '♙', '♙', '♙', '♙', '♙', '♙', '♙',
      '.', '♘', '♗', '♕', '♔', '♗', '♘', '♖'
    );
    let selected = "49";
    let result = get_promotion(&mut machine, &chessboard, &selected);

    assert_eq!(result, Some(String::from("♟♛♝♞♜")));

    let selected = "9";
    let result = get_promotion(&mut machine, &chessboard, &selected);

    assert!(result.is_none());
  }
}
