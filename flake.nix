{
  description = "test";
  inputs = {
    fenix.url = "github:nix-community/fenix";
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    crane.url = "github:ipetkov/crane";
  };
  outputs = { self, fenix, flake-utils, crane, nixpkgs } :
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      craneLib = crane.lib.${system};
      fox-src = builtins.path {
        path = "${./.}";
        filter = path: type:
          ! builtins.elem path (map (
              f: "${./.}/${f}"
            ) [
              "flake.nix"
              "flake.lock"
              "tests"
              "target"
              "pkgs"
            ]);
      };
      src = craneLib.path fox-src;
	    commonArgs = {
	      inherit src;
      	  buildInputs = with pkgs; [
            llvmPackages_16.clang-unwrapped
            llvmPackages_16.clang-unwrapped.lib
	          binaryen
	          wasm-bindgen-cli
	          wasm-pack
	          cargo-binutils
	          llvmPackages.lldb
	          lld
	        ];
	    };
	    craneLibLLvmTools = craneLib.overrideToolchain
        (with fenix.packages.${system}; combine [
              stable.rustc
              stable.cargo
              stable.rust-src
	            stable.llvm-tools-preview
              targets.wasm32-unknown-unknown.stable.rust-std
        ]);
      in rec {
        # Nix build
        defaultPackage = craneLibLLvmTools.buildPackage ( commonArgs // {
          LIBCLANG_PATH = "${pkgs.llvmPackages_16.clang-unwrapped.lib}/lib";
          installPhaseCommand = ''
	          mkdir -p $out/pkg

            export C_INCLUDE_PATH="$LIBCLANG_PATH/clang/16/include"
            CC=clang HOME=$(mktemp -d fake-homeXXXX) wasm-pack build -m no-install --target web --no-typescript --out-dir $out/pkg;
            cp --recursive web $out
            cp index.html $out
          '';
	      });
        # Nix develop
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            hello
          ];
        };
      }
    );
}
