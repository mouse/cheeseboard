importScripts('../pkg/cheeseboard_wasm.js');

console.log('Initializing worker')

const {MachineEval} = wasm_bindgen;

async function init() {
    await wasm_bindgen('../pkg/cheeseboard_wasm_bg.wasm');

    var game = MachineEval.start();

    self.onmessage = async event => {
      var worker_result = game.play(event.data);

      self.postMessage(worker_result);
    }
};

init();
