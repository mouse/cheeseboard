const {start} = wasm_bindgen;

async function run_cheeseboard() {
  await wasm_bindgen();

  await start();
}

run_cheeseboard();
