use serde::{Serialize, Deserialize};
use base64::{engine::general_purpose::{STANDARD, URL_SAFE}, Engine as _};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum MultiUpdate {
  Piece(Vec<u8>),
  Start(String),
  Mouvement(String, String),
  Castling(String, String, String, String),
  Passant(String, String, String),
  Promotion(String, String, String),
}

impl MultiUpdate {
  pub fn update(&self) {
    let window: web_sys::Window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    match self {
      Self::Promotion(id_from, id_to, piece) => {
        // Unset pawn
        let element_from = document.get_element_by_id(id_from).unwrap();
        element_from.set_text_content(Some(&"."));

        let element_cell = document.get_element_by_id(id_to).unwrap();
        element_cell.set_text_content(Some(&piece));

        super::State::push_state();
      },
      Self::Piece(slice) => {
        let encoded = STANDARD.encode(&slice);

        let font_set = document.fonts();
        let font_face = web_sys::FontFace::new_with_str(
          "Cheeseboard",
          &format!("url(data:font/ttf;charset=utf-8;base64,{}) format('opentype') tech(color-COLRv1)", encoded)
        ).unwrap();
        font_set.add(&font_face).unwrap();
      },
      Self::Start(serialized_state) => {
        let decoded_state = URL_SAFE.decode(serialized_state).unwrap();
        let bytes = String::from_utf8(decoded_state).unwrap();
        let state: super::State = serde_json::from_str(&bytes).unwrap();

        state.pull_state();
      },
      Self::Mouvement(id_from, id_to) => {
        let element_from = document.get_element_by_id(id_from).unwrap();
        let element_to = document.get_element_by_id(id_to).unwrap();

        let from_to_content = element_from.text_content().unwrap();
        element_from.set_text_content(Some(&"."));
        element_to.set_text_content(Some(&from_to_content));

        super::State::push_state();
      },
      Self::Passant(id_from_pawn, id_to_pawn, id_enemy) => {
        // Remove enemy
        let element_enemy = document.get_element_by_id(id_enemy).unwrap();
        element_enemy.set_text_content(Some(&"."));

        // Passant
        let element_from = document.get_element_by_id(id_from_pawn).unwrap();
        let element_to = document.get_element_by_id(id_to_pawn).unwrap();

        let from_to_content = element_from.text_content().unwrap();
        element_from.set_text_content(Some(&"."));
        element_to.set_text_content(Some(&from_to_content));

        super::State::push_state();
      },
      Self::Castling(id_from_king, id_to_king, id_from_tower, id_to_tower) => {
        vec![(id_from_king, id_to_king), (id_from_tower, id_to_tower)]
            .iter().for_each(|(id_from, id_to)| {
          let element_from = document.get_element_by_id(id_from).unwrap();
          let element_to = document.get_element_by_id(id_to).unwrap();

          let from_to_content = element_from.text_content().unwrap();
          element_from.set_text_content(Some(&"."));
          element_to.set_text_content(Some(&from_to_content));
        });
        super::State::push_state();
      },
    }
  }
}
