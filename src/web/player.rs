use std::str::FromStr;
use std::ops::Not;

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Player {
  White,
  Black
}

impl Not for Player {
  type Output = Self;

  fn not(self) -> Self {
    match self {
      Self::White => Self::Black,
      Self::Black => Self::White,
    }
  }
}

impl FromStr for Player {
  type Err = ();
  fn from_str(input: &str) -> Result<Self, Self::Err> {
    match input {
      "white" => Ok(Self::White),
      "black" => Ok(Self::Black),
      _ => Err(()),
    }
  }
}

impl ToString for Player {
  fn to_string(&self) -> String {
    match self {
      Self::White => String::from("white"),
      Self::Black => String::from("black"),
    }
  }
}
