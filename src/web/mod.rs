mod player;
mod state;
mod update;

pub use update::MultiUpdate;
pub use player::Player;
pub use state::State;

use std::str::FromStr;

use wasm_bindgen::prelude::*;

pub const PARAM_CHESS: &str = "chess";

pub fn get_chessboard(document: &web_sys::Document) -> String {
  document.get_element_by_id("chessboard").unwrap()
          .text_content().unwrap().chars().filter(|piece| match piece {
           '\u{2654}' ..= '\u{2659}' | '\u{265A}' ..= '\u{265F}' | '.' => true,
            _ => false,
          }).collect::<String>()
}

pub fn set_playable_cells(document: &web_sys::Document) {
  let element_chessboard = document.get_element_by_id("chessboard").unwrap();
  let data_player = element_chessboard.get_attribute("data-player").unwrap();
  let player = Player::from_str(&data_player).unwrap();
  (1..65).for_each(|index| {
    let element_cell = document.get_element_by_id(&index.to_string()).unwrap();
    let piece = element_cell.text_content().unwrap().chars().next().unwrap();
    match (piece, &player) {
      ('\u{2654}' ..= '\u{2659}', Player::White) |
      ('\u{265A}' ..= '\u{265F}', Player::Black) => {
        element_cell.set_attribute("draggable", "true").unwrap();
      },
      _ => {
        element_cell.set_attribute("draggable", "false").unwrap();
      },
    }
  });
}

pub fn has_to_play(document: &web_sys::Document) -> bool {
  let element_chessboard = document.get_element_by_id("chessboard").unwrap();
  let data_mover = element_chessboard.get_attribute("data-mover").unwrap();
  let data_player = element_chessboard.get_attribute("data-player").unwrap();

  data_mover == data_player
}

pub fn create_promotion(
  document: &web_sys::Document,
  promotions: &String
) -> web_sys::HtmlElement {
  let mut it_promotion =
    promotions.chars().map(|promotion| promotion.to_string());
  let element_select = document.create_element("select").unwrap();
  let element_select: web_sys::HtmlElement =
    element_select.dyn_ref::<web_sys::HtmlElement>().unwrap().clone();

  let option = document.create_element("option").unwrap();
  option.set_text_content(it_promotion.next().as_deref());
  option.set_attribute("selected", "selected").unwrap();
  option.set_attribute("disabled", "disabled").unwrap();
  element_select.append_child(&option).unwrap();
  it_promotion.for_each(|piece| {
    let option = document.create_element("option").unwrap();
    option.set_text_content(Some(&piece.to_string()));
    element_select.append_child(&option).unwrap();
  });
  element_select
}
