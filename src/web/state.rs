use std::str::FromStr;

use base64::{engine::general_purpose::URL_SAFE, Engine as _};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct State {
  chessboard: String,
  mover: super::Player
}

impl State {
  fn new() -> Self {
    let window: web_sys::Window = web_sys::window().unwrap();
    let document = window.document().unwrap();

    let chessboard: String = super::get_chessboard(&document);

    let element_chessboard = document.get_element_by_id("chessboard").unwrap();
    let data_mover = element_chessboard.get_attribute("data-mover").unwrap();
    let mover = super::Player::from_str(&data_mover).unwrap();

    Self {
      chessboard: chessboard,
      mover: mover,
    }
  }

  pub fn push_state() -> Self {
    let window: web_sys::Window = web_sys::window().unwrap();
    let history = window.history().unwrap();

    let state = Self::new().state_mover_reverse();

    let bytes = serde_json::to_string(&state.clone()).unwrap();
    let encoded_state: String = URL_SAFE.encode(bytes);

    history.push_state_with_url(
      &history.state().unwrap(), &String::default(),
      Some(&format!("?{}={}", super::PARAM_CHESS, encoded_state))
    ).unwrap();

    let document = window.document().unwrap();
    if super::has_to_play(&document) {
      super::set_playable_cells(&document);
    } else {
      // disable all cells
      (1..65).for_each(|index| {
        let element_cell = document.get_element_by_id(&index.to_string()).unwrap();
        element_cell.set_attribute("draggable", "false").unwrap();
      });
    }

    state
  }

  fn state_mover_reverse(mut self) -> Self {
    self.mover = !self.mover;
    self.pull_state_mover();
    self
  }

  fn pull_state_mover(&self) {
    let window: web_sys::Window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let element_chessboard = document.get_element_by_id("chessboard").unwrap();

    let mover = self.mover.to_string();
    element_chessboard.set_attribute("data-mover", &mover).unwrap();
  }

  pub fn pull_state(&self) {
    let window: web_sys::Window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    self.pull_state_mover();
    self.chessboard.chars().enumerate().for_each(|(index, content)| {
      let index = index + 1;
      document.get_element_by_id(&index.to_string()).unwrap()
              .set_text_content(Some(&content.to_string()));
    });

    if super::has_to_play(&document) {
      super::set_playable_cells(&document);
    }
  }
}
