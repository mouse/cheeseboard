:- use_module(chess, [target/4]).
:- use_module(library(between)).
:- use_module(library(lists)).

main :-
  IndexChars = "56",
  IdChars = "\xF0005\",
  number_chars(Index, IndexChars),
  atom_chars(Id, IdChars),

  Chessboard = "\xF0013\\xF0018\\xF0006\\xF000c\\xF0008\\xF0006\\xF0018\\xF0013\\xF000a\\xF000a\\xF000a\\xF000a\\xF000a\\xF000a\\xF000a\\xF000a\................................\xF0005\\xF0005\\xF0005\\xF0005\\xF0005\\xF0005\\xF0005\\xF0005\\xF0011\\xF000F\\xF0001\\xF0005\\xF0003\\xF0001\\xF000F\\xF0011\",
  numlist(1, 64, Enumboard),
  transpose([Chessboard, Enumboard], Board),
  
  findall(NextIndex, target(Board, Id, Index, NextIndex), Result),
  write(Result),

  true.

:- initialization(main).
