:- use_module(library(format)).
:- use_module(library(between)).
:- use_module(library(lists)).
:- use_module(library(clpz)).

is_capturable_white(Enemy) :-
  member(Enemy, ['♙', '♖', '♘', '♗', '♕']).

is_capturable_black(Enemy) :-
  member(Enemy, ['♟', '♜', '♞', '♝', '♛']).

is_empty_or_capturable_white(EnemyOrEmpty) :-
  member(EnemyOrEmpty, ['.', '♙', '♖', '♘', '♗', '♕']).

is_empty_or_capturable_black(EnemyOrEmpty) :-
  member(EnemyOrEmpty, ['.', '♟', '♜', '♞', '♝', '♛']).

is_white(Piece) :-
  member(Piece, ['♙', '♖', '♘', '♗', '♕', '♔']).

is_black(Piece) :-
  member(Piece, ['♟', '♜', '♞', '♝', '♛', '♚']).

to_end_top(Board, Piece, Index, Result, Acc) :-
  Index #> 8,
  is_white(Piece),
  NextIndex #= Index - 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc] ;
  Index #> 8,
  is_black(Piece),
  NextIndex #= Index - 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_end_bottom(Board, Piece, Index, Result, Acc) :-
  Index #> 8,
  is_white(Piece),
  NextIndex #= Index + 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc] ;
  Index #> 8,
  is_black(Piece),
  NextIndex #= Index + 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_end_right(Board, Piece, Index, Result, Acc) :-
  (Index mod 8) #\= 0, % Column !8
  is_white(Piece),
  NextIndex #= Index + 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc];
  (Index mod 8) #\= 0, % Column !8
  is_black(Piece),
  NextIndex #= Index + 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_end_left(Board, Piece, Index, Result, Acc) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  is_white(Piece),
  NextIndex #= Index - 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc] ;
  ((Index + 7) mod 8) #\= 0, % Column !1
  is_black(Piece),
  NextIndex #= Index - 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_top(Board, Piece, Index, Result, Acc) :-
  Index #> 8,
  NextIndex #= Index - 8,
  nth1(NextIndex, Board, ['.', _]),
  to_top(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_top(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

to_bottom(Board, Piece, Index, Result, Acc) :-
  Index #< 64,
  NextIndex #= Index + 8,
  nth1(NextIndex, Board, ['.', _]),
  to_bottom(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_bottom(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

to_right(Board, Piece, Index, Result, Acc) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex #= Index + 1,
  nth1(NextIndex, Board, ['.', _]),
  to_right(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_right(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

to_left(Board, Piece, Index, Result, Acc) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex #= Index - 1,
  nth1(NextIndex, Board, ['.', _]),
  to_left(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_left(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

to_end_top_left(Board, Piece, Index, Result, Acc) :-
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  is_white(Piece),
  NextIndex #= Index - 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc] ;
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  is_black(Piece),
  NextIndex #= Index - 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_end_top_right(Board, Piece, Index, Result, Acc) :-
  Index > 8,
  (Index mod 8) #\= 0, % Column !8
  is_white(Piece),
  NextIndex #= Index - 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc] ;
  Index > 8,
  (Index mod 8) #\= 0, % Column !8
  is_black(Piece),
  NextIndex #= Index - 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_end_bottom_left(Board, Piece, Index, Result, Acc) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  is_white(Piece),
  NextIndex #= Index + 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc] ;
  ((Index + 7) mod 8) #\= 0, % Column !1
  is_black(Piece),
  NextIndex #= Index + 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_end_bottom_right(Board, Piece, Index, Result, Acc) :-
  (Index mod 8) #\= 0, % Column !8
  is_white(Piece),
  NextIndex #= Index + 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy),
  Result = [NextIndex|Acc] ;
  (Index mod 8) #\= 0, % Column !8
  is_black(Piece),
  NextIndex #= Index + 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy),
  Result = [NextIndex|Acc].

to_top_left(Board, Piece, Index, Result, Acc) :-
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 9,
  nth1(NextIndex, Board, ['.', _]),
  to_top_left(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_top_left(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

to_top_right(Board, Piece, Index, Result, Acc) :-
  Index > 8,
  (Index mod 8) #\= 0, % Column !8
  NextIndex #= Index - 7,
  nth1(NextIndex, Board, ['.', _]),
  to_top_right(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_top_right(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

to_bottom_left(Board, Piece, Index, Result, Acc) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex #= Index + 7,
  nth1(NextIndex, Board, ['.', _]),
  to_bottom_left(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_bottom_left(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

to_bottom_right(Board, Piece, Index, Result, Acc) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex #= Index + 9,
  nth1(NextIndex, Board, ['.', _]),
  to_bottom_right(Board, Piece, NextIndex, Result, [NextIndex|Acc]),
  !;
  to_end_bottom_right(Board, Piece, Index, Result, Acc),
  !;
  Result = Acc.

% Rook
target(Board, '♖', Index, NextIndex) :-
  to_top(Board, '♖', Index, ResultTop, []),
  to_bottom(Board, '♖', Index, ResultBottom, []),
  to_right(Board, '♖', Index, ResultRight, []),
  to_left(Board, '♖', Index, ResultLeft, []),
  append([ResultTop, ResultBottom, ResultRight, ResultLeft], NextIndex).

target(Board, '♜', Index, NextIndex) :-
  to_top(Board, '♜', Index, ResultTop, []),
  to_bottom(Board, '♜', Index, ResultBottom, []),
  to_right(Board, '♜', Index, ResultRight, []),
  to_left(Board, '♜', Index, ResultLeft, []),
  append([ResultTop, ResultBottom, ResultRight, ResultLeft], NextIndex).

% Bishop
target(Board, '♗', Index, NextIndex) :-
  to_top_left(Board, '♗', Index, ResultTopLeft, []),
  to_top_right(Board, '♗', Index, ResultTopRight, []),
  to_bottom_left(Board, '♗', Index, ResultBottomLeft, []),
  to_bottom_right(Board, '♗', Index, ResultBottomRight, []),
  append([ResultTopLeft, ResultTopRight,
          ResultBottomLeft, ResultBottomRight], NextIndex).

% Bishop
target(Board, '♝', Index, NextIndex) :-
  to_top_left(Board, '♝', Index, ResultTopLeft, []),
  to_top_right(Board, '♝', Index, ResultTopRight, []),
  to_bottom_left(Board, '♝', Index, ResultBottomLeft, []),
  to_bottom_right(Board, '♝', Index, ResultBottomRight, []),
  append([ResultTopLeft, ResultTopRight,
          ResultBottomLeft, ResultBottomRight], NextIndex).

% Queen
target(Board, '♕', Index, NextIndex) :-
  to_top(Board, '♕', Index, ResultTop, []),
  to_top_left(Board, '♕', Index, ResultTopLeft, []),
  to_top_right(Board, '♕', Index, ResultTopRight, []),
  to_bottom(Board, '♕', Index, ResultBottom, []),
  to_bottom_left(Board, '♕', Index, ResultBottomLeft, []),
  to_bottom_right(Board, '♕', Index, ResultBottomRight, []),
  to_right(Board, '♕', Index, ResultRight, []),
  to_left(Board, '♕', Index, ResultLeft, []),
  append([ResultTop, ResultTopLeft, ResultTopRight,
          ResultBottom, ResultBottomLeft, ResultBottomRight,
	  ResultRight, ResultLeft], NextIndex).

target(Board, '♛', Index, NextIndex) :-
  to_top(Board, '♛', Index, ResultTop, []),
  to_top_left(Board, '♛', Index, ResultTopLeft, []),
  to_top_right(Board, '♛', Index, ResultTopRight, []),
  to_bottom(Board, '♛', Index, ResultBottom, []),
  to_bottom_left(Board, '♛', Index, ResultBottomLeft, []),
  to_bottom_right(Board, '♛', Index, ResultBottomRight, []),
  to_right(Board, '♛', Index, ResultRight, []),
  to_left(Board, '♛', Index, ResultLeft, []),
  append([ResultTop, ResultTopLeft, ResultTopRight,
          ResultBottom, ResultBottomLeft, ResultBottomRight,
	  ResultRight, ResultLeft], NextIndex).

% Knight White Top Right
target(Board, '♘', Index, NextIndex) :-
  Index > 16,
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index - 15,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight White Top Left
target(Board, '♘', Index, NextIndex) :-
  Index > 16,
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 17,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight White Right Top
target(Board, '♘', Index, NextIndex) :-
  Index > 8,
  ((Index + 1) mod 8) #\= 0, % Column !7
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index - 6,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight White Left Top
target(Board, '♘', Index, NextIndex) :-
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  ((Index + 6) mod 8) #\= 0, % Column !2
  NextIndex is Index - 10,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight White Bottom Right
target(Board, '♘', Index, NextIndex) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 17,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight White Bottom Left
target(Board, '♘', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index + 15,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight White Right Bottom
target(Board, '♘', Index, NextIndex) :-
  ((Index + 1) mod 8) #\= 0, % Column !7
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 10,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight White Left Bottom
target(Board, '♘', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  ((Index + 6) mod 8) #\= 0, % Column !2
  NextIndex is Index + 6,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% Knight Black Top Right
target(Board, '♞', Index, NextIndex) :-
  Index > 16,
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 17,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% Knight Black Top Left
target(Board, '♞', Index, NextIndex) :-
  Index > 16,
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index - 15,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% Knight Black Right Top
target(Board, '♞', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  ((Index + 6) mod 8) #\= 0, % Column !2
  NextIndex is Index + 6,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% Knight Black Left Top
target(Board, '♞', Index, NextIndex) :-
  ((Index + 1) mod 8) #\= 0, % Column !7
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 10,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% Knight Black Bottom Right
target(Board, '♞', Index, NextIndex) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 17,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% Knight Black Bottom Left
target(Board, '♞', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index + 15,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% Knight Black Right Bottom
target(Board, '♞', Index, NextIndex) :-
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  ((Index + 6) mod 8) #\= 0, % Column !2
  NextIndex is Index - 10,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% Knight Black Left Bottom
target(Board, '♞', Index, NextIndex) :-
  Index > 8,
  ((Index + 1) mod 8) #\= 0, % Column !7
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index - 6,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King White Top
target(Board, '♔', Index, NextIndex) :-
  Index > 8,
  NextIndex is Index - 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Left
target(Board, '♔', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Top Left
target(Board, '♔', Index, NextIndex) :-
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Top Right
target(Board, '♔', Index, NextIndex) :-
  Index > 8,
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index - 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Right
target(Board, '♔', Index, NextIndex) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Bottom
target(Board, '♔', Index, NextIndex) :-
  NextIndex is Index + 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Bottom Left
target(Board, '♔', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index + 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Bottom Right
target(Board, '♔', Index, NextIndex) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_black(Enemy).

% King White Castling Right
target(Board, '♔', Index, NextIndex) :-
  Index == 61,
  nth1(62, Board, ['.', _]),
  nth1(63, Board, ['.', _]),
  nth1(64, Board, ['♖', _]),
  NextIndex is 63.

% King White Castling Right
target(Board, '♔', Index, NextIndex) :-
  Index == 61,
  nth1(60, Board, ['.', _]),
  nth1(59, Board, ['.', _]),
  nth1(58, Board, ['.', _]),
  nth1(57, Board, ['♖', _]),
  NextIndex is 59.

% King Black Top
target(Board, '♚', Index, NextIndex) :-
  Index > 8,
  NextIndex is Index - 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Left
target(Board, '♚', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Top Left
target(Board, '♚', Index, NextIndex) :-
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Top Right
target(Board, '♚', Index, NextIndex) :-
  Index > 8,
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index - 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Right
target(Board, '♚', Index, NextIndex) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 1,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Bottom
target(Board, '♚', Index, NextIndex) :-
  NextIndex is Index + 8,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Bottom Left
target(Board, '♚', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index + 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Bottom Right
target(Board, '♚', Index, NextIndex) :-
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_empty_or_capturable_white(Enemy).

% King Black Castling Right
target(Board, '♚', Index, NextIndex) :-
  Index == 5,
  nth1(6, Board, ['.', _]),
  nth1(7, Board, ['.', _]),
  nth1(8, Board, ['♜', _]),
  NextIndex is 7.

% King Black Castling Right
target(Board, '♚', Index, NextIndex) :-
  Index == 5,
  nth1(4, Board, ['.', _]),
  nth1(3, Board, ['.', _]),
  nth1(2, Board, ['.', _]),
  nth1(1, Board, ['♜', _]),
  NextIndex is 3.

% Pawn White Mouvement
target(Board, '♙', Index, NextIndex) :-
  Index > 8,
  NextIndex is Index - 8,
  nth1(NextIndex, Board, ['.', _]).

% Pawn White Double Mouvement
target(Board, '♙', Index, NextIndex) :-
  Index > 48,
  Index < 57,
  BeforeIndex is Index - 8,
  nth1(BeforeIndex, Board, ['.', _]),
  NextIndex is Index - 16,
  nth1(NextIndex, Board, ['.', _]).

% Pawn White Capture
target(Board, '♙', Index, NextIndex) :-
  Index > 8,
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index - 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy);
  Index > 8,
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index - 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_black(Enemy).

% Pawn White Capture Passant
target(Board, '♙', Index, NextIndex) :-
  Index > 25,
  Index < 33,
  BeforeNextIndex is Index - 1,
  nth1(BeforeNextIndex, Board, ['♟', _]),
  NextIndex is Index - 9,
  nth1(NextIndex, Board, ['.', _]);
  Index > 24,
  Index < 32,
  BeforeNextIndex is Index + 1,
  nth1(BeforeNextIndex, Board, ['♟', _]),
  NextIndex is Index - 7,
  nth1(NextIndex, Board, ['.', _]).

% Pawn Black Mouvement
target(Board, '♟', Index, NextIndex) :-
  NextIndex is Index + 8,
  nth1(NextIndex, Board, ['.', _]).

% Pawn Black Double Mouvement
target(Board, '♟', Index, NextIndex) :-
  Index > 8,
  Index < 17,
  BeforeIndex is Index + 8,
  nth1(BeforeIndex, Board, ['.', _]),
  NextIndex is Index + 16,
  nth1(NextIndex, Board, ['.', _]).

% Pawn Black Capture
target(Board, '♟', Index, NextIndex) :-
  ((Index + 7) mod 8) #\= 0, % Column !1
  NextIndex is Index + 7,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy);
  (Index mod 8) #\= 0, % Column !8
  NextIndex is Index + 9,
  nth1(NextIndex, Board, [Enemy, _]),
  is_capturable_white(Enemy).

% Pawn Black Capture Passant
target(Board, '♟', Index, NextIndex) :-
  Index > 32,
  Index < 40,
  BeforeNextIndex is Index + 1,
  nth1(BeforeNextIndex, Board, ['♙', _]),
  NextIndex is Index + 9,
  nth1(NextIndex, Board, ['.', _]);
  Index > 33,
  Index < 41,
  BeforeNextIndex is Index - 1,
  nth1(BeforeNextIndex, Board, ['♙', _]),
  NextIndex is Index + 7,
  nth1(NextIndex, Board, ['.', _]).

get_marks(Chessboard, IdIndex, Result) :-
  numlist(1, 64, Enumboard),
  transpose([Chessboard, Enumboard], Board),

  select([Id, IdIndex], Board, _),

  findall(NextIndex, target(Board, Id, IdIndex, NextIndex), Result).

target_castling(Board, '♔', Index, NextIndex, SideIndex) :-
  Index == 61,
  nth1(62, Board, ['.', _]),
  nth1(63, Board, ['.', _]),
  nth1(64, Board, ['♖', _]),
  NextIndex is 63,
  SideIndex = [64, 62].

target_castling(Board, '♔', Index, NextIndex, SideIndex) :-
  Index == 61,
  nth1(60, Board, ['.', _]),
  nth1(59, Board, ['.', _]),
  nth1(58, Board, ['.', _]),
  nth1(57, Board, ['♖', _]),
  NextIndex is 59,
  SideIndex = [57, 60].

target_castling(Board, '♚', Index, NextIndex, SideIndex) :-
  Index == 5,
  nth1(6, Board, ['.', _]),
  nth1(7, Board, ['.', _]),
  nth1(8, Board, ['♜', _]),
  NextIndex is 7,
  SideIndex = [8, 6].

target_castling(Board, '♚', Index, NextIndex, SideIndex) :-
  Index == 5,
  nth1(4, Board, ['.', _]),
  nth1(3, Board, ['.', _]),
  nth1(2, Board, ['.', _]),
  nth1(1, Board, ['♜', _]),
  NextIndex is 3,
  SideIndex = [1, 4].

get_tower_castling(Chessboard, PreviousIndex, Index, Result) :-
  numlist(1, 64, Enumboard),
  transpose([Chessboard, Enumboard], Board),
  nth1(PreviousIndex, Board, [King, _]),
  target_castling(Board, King, PreviousIndex, Index, Result).

target_passant(Board, '♙', Index, PreviousIndex, SideIndex) :-
  Index > 25,
  Index < 33,
  BeforeNextIndex is Index - 1,
  nth1(BeforeNextIndex, Board, ['♟', SideIndex]),
  NextIndex is Index - 9,
  nth1(NextIndex, Board, ['.', PreviousIndex]);
  Index > 24,
  Index < 32,
  BeforeNextIndex is Index + 1,
  nth1(BeforeNextIndex, Board, ['♟', SideIndex]),
  NextIndex is Index - 7,
  nth1(NextIndex, Board, ['.', PreviousIndex]).

target_passant(Board, '♟', Index, PreviousIndex, SideIndex) :-
  Index > 32,
  Index < 40,
  BeforeNextIndex is Index + 1,
  nth1(BeforeNextIndex, Board, ['♙', SideIndex]),
  NextIndex is Index + 9,
  nth1(NextIndex, Board, ['.', PreviousIndex]);
  Index > 33,
  Index < 41,
  BeforeNextIndex is Index - 1,
  nth1(BeforeNextIndex, Board, ['♙', SideIndex]),
  NextIndex is Index + 7,
  nth1(NextIndex, Board, ['.', PreviousIndex]).

get_pawn_passant(Chessboard, PreviousIndex, Index, Result) :-
  numlist(1, 64, Enumboard),
  transpose([Chessboard, Enumboard], Board),
  nth1(PreviousIndex, Board, [Pawn, _]),
  target_passant(Board, Pawn, PreviousIndex, Index, Result).

target_promotion(Board, Index, Result) :-
  Index < 17,
  nth1(Index, Board, ['♙', _]),
  Result = ['♙', '♕', '♗', '♘', '♖'].

target_promotion(Board, Index, Result) :-
  Index > 48,
  nth1(Index, Board, ['♟', _]),
  Result = ['♟', '♛', '♝', '♞', '♜'].

get_promotion(Chessboard, Index, Result) :-
  numlist(1, 64, Enumboard),
  transpose([Chessboard, Enumboard], Board),
  target_promotion(Board, Index, Result).
