use wasm_bindgen::prelude::*;
use wasm_bindgen::JsValue;
use wasm_bindgen::closure::Closure;
use serde::{Serialize, Deserialize};

use base64::{engine::general_purpose::URL_SAFE, Engine as _};

const STUN_SERVER: &str = "stun:stun.l.google.com:19302";
pub const PARAM_CODE: &str = "code";

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RtcSdpType(usize);

impl From<web_sys::RtcSdpType> for RtcSdpType {
  fn from(type_: web_sys::RtcSdpType) -> Self {
    match type_ {
      web_sys::RtcSdpType::Offer => RtcSdpType(0),
      web_sys::RtcSdpType::Pranswer => RtcSdpType(1),
      web_sys::RtcSdpType::Answer => RtcSdpType(2),
      web_sys::RtcSdpType::Rollback => RtcSdpType(3),
      _ => unimplemented!(),
    }
  }
}

impl Into<web_sys::RtcSdpType> for RtcSdpType {
  fn into(self) -> web_sys::RtcSdpType {
    match self {
      Self(0) => web_sys::RtcSdpType::Offer,
      Self(1) => web_sys::RtcSdpType::Pranswer,
      Self(2) => web_sys::RtcSdpType::Answer,
      Self(3) => web_sys::RtcSdpType::Rollback,
      _ => unimplemented!(),
    }
  }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Description {
  type_: RtcSdpType,
  uflag: String,
  sdp: webrtc_sdp::SdpSession,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Code {
  description: Description,
}

fn exception_handler<M>(msg: M) -> Closure<dyn FnMut(JsValue)>
where M: std::fmt::Display + 'static,
{
  Closure::new(move |value: JsValue| {
    web_sys::console::error_1(&format!("why {} {:?}", msg, value).into());
  })
}

fn peer_connection() -> Result<web_sys::RtcPeerConnection, JsValue> {
  let url = js_sys::Object::new();
  js_sys::Reflect::set(&url, &"urls".into(), &STUN_SERVER.into())?;

  let ice_servers = js_sys::Array::new();
  ice_servers.push(&url);

  let mut configuration = web_sys::RtcConfiguration::new();
  configuration.ice_servers(&ice_servers);

  let pc = web_sys::RtcPeerConnection::new_with_configuration(&configuration)?;

  Ok(pc)
}

fn init_channel(pc: &web_sys::RtcPeerConnection) ->
  Option<web_sys::RtcDataChannel> {
  let window: web_sys::Window = web_sys::window().unwrap();
  let document = window.document().unwrap();
  let mut data_channel_dict: web_sys::RtcDataChannelInit =
    web_sys::RtcDataChannelInit::new();

  data_channel_dict.id(0);
  data_channel_dict.negotiated(true);
  data_channel_dict.ordered(true);
  
  let data_channel: web_sys::RtcDataChannel =
    pc.create_data_channel_with_data_channel_dict("chat", &data_channel_dict);

  let data_channel_clone = data_channel.clone();
  let document_clone = document.clone();
  let on_open_closure = Closure::wrap(Box::new(move |_: wasm_bindgen::JsValue| {
    let query = window.location().search().unwrap();
    let params = web_sys::UrlSearchParams::new_with_str(&query).unwrap();
    if let Some(encoded_state) = params.get(super::web::PARAM_CHESS) {
      let start = super::web::MultiUpdate::Start(encoded_state.clone());
      let bytes = serde_json::to_string(&start).unwrap();

      data_channel_clone.send_with_str(&bytes).unwrap();
    } else {
      super::web::set_playable_cells(&document_clone);
    }

    let element_main_webrtc = document_clone.get_element_by_id("webrtc").unwrap();
    element_main_webrtc.set_attribute(&"hidden", &"hidden").unwrap();
    let element_main_chessboard = document_clone.get_element_by_id("chessboard").unwrap();
    element_main_chessboard.remove_attribute(&"hidden").unwrap();
  }) as Box<dyn FnMut(_)>);

  let document_clone = document.clone();
  let on_close_closure = Closure::wrap(Box::new(move || {
    let element_main_chessboard = document_clone.get_element_by_id("chessboard").unwrap();
    element_main_chessboard.set_attribute("disabled", "disabled").unwrap();
  }) as Box<dyn FnMut()>);
  let on_message_closure = Closure::wrap(Box::new(move |event: wasm_bindgen::JsValue| {
    let message_event = event.unchecked_into::<web_sys::MessageEvent>();
    let bytes = message_event.data().as_string().unwrap();
    let multi: super::web::MultiUpdate = serde_json::from_str(&bytes).unwrap();

    multi.update();
  }) as Box<dyn FnMut(_)>);
  data_channel.set_onopen(Some(on_open_closure.as_ref().unchecked_ref()));
  data_channel.set_onclose(Some(on_close_closure.as_ref().unchecked_ref()));
  data_channel.set_onmessage(Some(on_message_closure.as_ref().unchecked_ref()));
  on_open_closure.forget();
  on_close_closure.forget();
  on_message_closure.forget();

  Some(data_channel)
}

fn set_offer(
  pc: &web_sys::RtcPeerConnection,
  code: Code
) -> Result<(), JsValue> {
  let bytes = serde_columnar::to_vec(&code).unwrap();
  let encoded: String = URL_SAFE.encode(bytes);

  let window: web_sys::Window = web_sys::window().unwrap();
  let document = window.document().unwrap();

  let element_offer = document.get_element_by_id("offer").unwrap();
  let input_element_offer = element_offer
      .dyn_ref::<web_sys::HtmlInputElement>().unwrap();
  let location = window.location();
  input_element_offer.set_attribute(&"value", &format!("{}{}?{}={}",
    location.origin()?,
    location.pathname()?,
    PARAM_CODE,
    &encoded
  ))?;
  let on_focus_closure = Closure::wrap(Box::new(move |event: web_sys::Event| {
    let input_element_offer: web_sys::HtmlInputElement =
      event.target().unwrap()
           .dyn_ref::<web_sys::HtmlInputElement>().unwrap().clone();
    input_element_offer.select();
  }) as Box<dyn FnMut(_)>);
  input_element_offer.set_onfocus(Some(on_focus_closure.as_ref().unchecked_ref()));
  on_focus_closure.forget();

  let element_answer = document.get_element_by_id("answer").unwrap();
  let input_element_answer =
    element_answer.dyn_ref::<web_sys::HtmlInputElement>().unwrap();
  let pc_clone = pc.clone();
  let on_paste = Closure::wrap(Box::new(move |clip: web_sys::ClipboardEvent| {
    if let Some(serialized_code) = clip.clipboard_data().unwrap().get_data("text").ok() {
      let decoded_code = URL_SAFE.decode(serialized_code.trim()).unwrap();
      let code: Code = serde_columnar::from_bytes::<Code>(decoded_code.as_slice()).unwrap();
      
      has_code(&pc_clone, code);
    }
  }) as Box<dyn FnMut(_)>);
  input_element_answer.set_onpaste(Some(on_paste.as_ref().unchecked_ref()));
  on_paste.forget();
  Ok(())
}

fn set_answer(code: Code) -> Result<(), JsValue> {
  let bytes = serde_columnar::to_vec(&code).unwrap();
  let encoded: String = URL_SAFE.encode(bytes);

  let window: web_sys::Window = web_sys::window().unwrap();
  let document = window.document().unwrap();

  let element_offer = document.get_element_by_id("offer-set").unwrap();
  element_offer.set_attribute(&"disabled", &"disabled")?;

  let element_answer_paste = document.get_element_by_id("answer-paste").unwrap();
  element_answer_paste.set_attribute(&"hidden", &"hidden")?;
  let element_answer_copy = document.get_element_by_id("answer-copy").unwrap();
  element_answer_copy.remove_attribute(&"hidden")?;

  let element_answer = document.get_element_by_id("answer").unwrap();
  let input_element_answer = element_answer
      .dyn_ref::<web_sys::HtmlInputElement>().unwrap();
  input_element_answer.set_attribute(&"value", &encoded)?;
  input_element_answer.set_attribute(&"readonly", &"readonly")?;
  let on_focus_closure = Closure::wrap(Box::new(move |event: web_sys::Event| {
    let input_element_offer: web_sys::HtmlInputElement =
      event.target().unwrap()
           .dyn_ref::<web_sys::HtmlInputElement>().unwrap().clone();
    input_element_offer.select();
  }) as Box<dyn FnMut(_)>);
  input_element_answer.set_onfocus(Some(on_focus_closure.as_ref().unchecked_ref()));
  on_focus_closure.forget();
  Ok(())
}

fn generate_code(pc: &web_sys::RtcPeerConnection) {
  let pc_clone = pc.clone();
  let create_offer_closure = Closure::wrap(Box::new(move |offer: wasm_bindgen::JsValue| {
    let description_init: web_sys::RtcSessionDescriptionInit =
        web_sys::RtcSessionDescriptionInit::try_from(offer).unwrap();
    let _promise = pc_clone
        .set_local_description(&description_init)
        .catch(&exception_handler("set_local_description failed"));
  }) as Box<dyn FnMut(_)>);
  let _promise =
    pc.create_offer()
      .then(&create_offer_closure)
      .catch(&exception_handler("create_offer failed"));
  create_offer_closure.forget();
}

fn generate_code_answer(pc: &web_sys::RtcPeerConnection) {
  let pc_clone = pc.clone();
  let create_description_closure = Closure::wrap(Box::new(move |offer: wasm_bindgen::JsValue| {
    let description_init: web_sys::RtcSessionDescriptionInit =
        web_sys::RtcSessionDescriptionInit::try_from(offer).unwrap();
      let _promise =
        pc_clone.set_local_description(&description_init)
        .catch(&exception_handler("local_description failed"));
  }) as Box<dyn FnMut(_)>);
  let _promise =
    pc.create_answer()
      .then(&create_description_closure)
      .catch(&exception_handler("create_answer failed"));
  create_description_closure.forget();
}

fn get_candidates(sdp: &webrtc_sdp::SdpSession, uflag: String)
  -> Vec<web_sys::RtcIceCandidateInit> {
  sdp.media.iter().map(|media| {
    media.get_attributes_of_type(webrtc_sdp::attribute_type::SdpAttributeType::Candidate)
      .iter().map(|attribute: &&webrtc_sdp::attribute_type::SdpAttribute| {
        match attribute {
          webrtc_sdp::attribute_type::SdpAttribute::Candidate(candidate) => {
            let mut candidate: webrtc_sdp::attribute_type::SdpAttributeCandidate =
              candidate.clone();

            candidate.ufrag = Some(uflag.clone());

            let mut candidate_init = web_sys::RtcIceCandidateInit::new(&format!("{}:{}",
              webrtc_sdp::attribute_type::SdpAttributeType::Candidate,
              candidate
            ));
            candidate_init.sdp_mid(Some(&"0"));
            candidate_init.sdp_m_line_index(Some(0));

            candidate_init
          },
          _ => unimplemented!()
        }
      }).collect::<Vec<web_sys::RtcIceCandidateInit>>()
  }).next().unwrap()
}

fn has_code(pc: &web_sys::RtcPeerConnection, code: Code) {
  let type_: web_sys::RtcSdpType = code.description.type_.into();
  let sdp: webrtc_sdp::SdpSession = code.description.sdp;
  let uflag: String = code.description.uflag;
  let candidate_inits = get_candidates(&sdp, uflag);
  let mut description_init: web_sys::RtcSessionDescriptionInit =
    web_sys::RtcSessionDescriptionInit::new(type_);

  description_init.sdp(&sdp.to_string());

  let pc_clone = pc.clone();
  let create_answer_closure = Closure::wrap(Box::new(move |_| {
    candidate_inits.iter().for_each(|candidate_init| {
      let candidate_init = candidate_init.clone();
      let _promise = pc_clone
        .add_ice_candidate_with_opt_rtc_ice_candidate_init(Some(&candidate_init))
        .catch(&exception_handler("add_ice_candidate failed"));
    });
    if type_ == web_sys::RtcSdpType::Offer {
      generate_code_answer(&pc_clone);
    }
  }) as Box<dyn FnMut(_)>);
  let _promise =
    pc.set_remote_description(&description_init)
      .then(&create_answer_closure)
      .catch(&exception_handler("create_answer failed"));
  create_answer_closure.forget();
}

pub async fn start_webrtc() -> Result<web_sys::RtcDataChannel, JsValue> {
  let pc: web_sys::RtcPeerConnection = peer_connection().unwrap();

  let data_channel: web_sys::RtcDataChannel = init_channel(&pc).unwrap();

  let pc_clone = pc.clone();
  let on_icecandidate_closure = Closure::wrap(Box::new(move |event: JsValue| {
    let ice_event: web_sys::RtcPeerConnectionIceEvent =
      event.unchecked_into::<web_sys::RtcPeerConnectionIceEvent>();
    if ice_event.candidate() == None {
      let description: web_sys::RtcSessionDescription =
        pc_clone.local_description().unwrap();
      let type_ = description.type_().into();
      let sdp = webrtc_sdp::parse_sdp(&description.sdp(), true).unwrap();
      let code = Code {
        description: Description {
          type_: type_,
          sdp: sdp.clone(),
          uflag: sdp.media.iter().map(|media| {
            media.get_attribute(webrtc_sdp::attribute_type::SdpAttributeType::IceUfrag)
                 .and_then(|attribute: &webrtc_sdp::attribute_type::SdpAttribute| {
                   match attribute {
                     webrtc_sdp::attribute_type::SdpAttribute::IceUfrag(uflag)
                       => Some(uflag.clone()),
                     _ => unimplemented!(),
                   }
                 }).unwrap()
          }).next().unwrap()
        },
      };

      if description.type_() == web_sys::RtcSdpType::Offer {
        set_offer(&pc_clone, code).unwrap();
      } else if description.type_() == web_sys::RtcSdpType::Answer {
        set_answer(code).unwrap();
      }
    }
  }) as Box<dyn FnMut(_)>);
  pc.set_onicecandidate(Some(on_icecandidate_closure.as_ref().unchecked_ref()));
  on_icecandidate_closure.forget();

  let pc_clone = pc.clone();
  let on_negotiationneeded_closure = Closure::wrap(Box::new(move |_: JsValue| {
    let window: web_sys::Window = web_sys::window().unwrap();

    let query = window.location().search().unwrap();
    let params = web_sys::UrlSearchParams::new_with_str(&query).unwrap();
  
    if let Some(serialized_code) = params.get(PARAM_CODE) {
      let decoded_code = URL_SAFE.decode(serialized_code).unwrap();
      let code: Code = serde_columnar::from_bytes::<Code>(decoded_code.as_slice()).unwrap();

      has_code(&pc_clone, code);
    } else {
      generate_code(&pc_clone);
    }
  }) as Box<dyn FnMut(_)>);
  pc.set_onnegotiationneeded(Some(on_negotiationneeded_closure.as_ref().unchecked_ref()));
  on_negotiationneeded_closure.forget();

  Ok(data_channel)
}
