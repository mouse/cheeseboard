use num_traits::ToPrimitive;

use wasm_bindgen::JsValue;
use wasm_bindgen::prelude::wasm_bindgen;

use scryer_prolog::machine::Machine;
use scryer_prolog::machine::parsed_results::{QueryResolution, QueryMatch, Value};

use serde::{Serialize, Deserialize};

use super::web::MultiUpdate;

static CODE: &str =
  include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/src/", "chess.pl"));

#[derive(Serialize, Deserialize, Debug)]
pub enum Update {
  Mark(String, Vec<String>),
  Promote(String, String, String),
  Clear,
  Multi(MultiUpdate)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Query {
  pub chessboard: String,
  pub selected: String,
}

#[derive(Debug, Clone)]
pub struct PreviousResult {
  pub result: Vec<String>,
  pub selected: String,
}

#[wasm_bindgen]
pub struct MachineEval {
  machine: Machine,
  previous_result: Option<PreviousResult>,
}

#[wasm_bindgen]
impl MachineEval {
  pub fn start() -> MachineEval {
    let mut machine = Machine::new_lib();

    web_sys::console::time_with_label("scryer-prolog");
    machine.load_module_string("facts", CODE.to_string());
    web_sys::console::time_end_with_label("scryer-prolog");

    MachineEval {
      machine: machine,
      previous_result: None
    }
  }

  fn get_marks(&mut self,
    chessboard: &String,
    selected: &String
  ) -> Vec<String> {
    if let Ok(QueryResolution::Matches(query_matches)) =
      self.machine.run_query(format!("get_marks(\"{}\", {}, Result).",
        chessboard, selected
    )) {
      if let Some((_, Value::List(values))) =
        query_matches
          .first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
        return values.iter().filter_map(|value: &Value| match value {
          Value::Float(ref value) =>
            value.to_usize().and_then(|value| Some(vec![value.to_string()])),
          Value::List(ref values) =>
            Some(values.iter().filter_map(|value: &Value| match value {
              Value::Float(ref value) =>
                value.to_usize()
                     .and_then(|value| Some(value.to_string())),
              _ => None
            }).collect::<Vec<String>>()),
          _ => None,
        }).flatten().collect::<Vec<String>>()
      }
    }
    unimplemented!()
  }

  fn get_tower_castling(&mut self,
    chessboard: &str,
    previous_selected: &str,
    selected: &str
  ) -> Option<(String, String)> {
    match self.machine.run_query(
      format!("get_tower_castling(\"{}\", {}, {}, Result).",
      chessboard, previous_selected, selected
    )) {
      Ok(QueryResolution::Matches(query_matches)) => {
        match query_matches.first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
          Some((_, Value::List(values))) => {
            let mut list = values.iter();
            if let [Some(Value::Float(value_from)),
                    Some(Value::Float(value_to))] = [list.next(), list.next()] {
              return Some((value_from.to_string(), value_to.to_string()))
            }
          },
          _ => {},
        }
      },
      Ok(_) => return None,
      _ => {},
    }
    unimplemented!()
  }

  fn get_pawn_passant(&mut self,
    chessboard: &str,
    selected: &str,
    previous_selected: &str
  ) -> Option<String> {
    match self.machine.run_query(
      format!("get_pawn_passant(\"{}\", {}, {}, Result).",
      chessboard, previous_selected, selected
    )) {
      Ok(QueryResolution::Matches(query_matches)) => {
        match query_matches.first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
          Some((_, Value::Float(value))) => {
            return Some(value.to_string())
          },
          _ => {},
        }
      },
      Ok(_) => return None,
      _ => {},
    }
    unimplemented!()
  }

  fn get_promotion(&mut self,
    chessboard: &str,
    selected: &str
  ) -> Option<String> {
    match self.machine.run_query(
      format!("get_promotion(\"{}\", {}, Result).",
      chessboard, selected
    )) {
      Ok(QueryResolution::Matches(query_matches)) => {
        match query_matches.first()
          .and_then(|QueryMatch { bindings }|
            bindings.get_key_value(&String::from("Result"))) {
          Some((_, Value::String(value))) => {
            return Some(value.to_string())
          },
          _ => {},
        }
      },
      Ok(_) => return None,
      _ => {},
    }
    unimplemented!()
  }

  pub fn play(&mut self, data_query: JsValue) -> Result<JsValue, JsValue> {
    let query: Query = serde_wasm_bindgen::from_value(data_query.clone())?; 
    match self.previous_result.clone() {
      Some(PreviousResult {
        ref selected, ..
      }) if selected == &query.selected => {
        self.previous_result = None;
        Ok(serde_wasm_bindgen::to_value(&Update::Clear)?)
      },
      Some(PreviousResult {
        ref result, ref selected
      }) if result.contains(&query.selected) => {
        let selected = selected.clone();
        self.previous_result = None;
        if let Some((from_tower, to_tower)) = self.get_tower_castling(
          &query.chessboard, &selected, &query.selected
        ) {
          Ok(serde_wasm_bindgen::to_value(&Update::Multi(MultiUpdate::Castling(
            selected, query.selected, from_tower, to_tower
          )))?)
        } else if let Some(enemy_pawn) = self.get_pawn_passant(
          &query.chessboard, &query.selected, &selected
        ) {
          Ok(serde_wasm_bindgen::to_value(&Update::Multi(MultiUpdate::Passant(
            selected, query.selected, enemy_pawn
          )))?)
        } else if let Some(promotions) = self.get_promotion(
          &query.chessboard, &selected
        ) {
          Ok(serde_wasm_bindgen::to_value(&Update::Promote(
            selected, query.selected, promotions
          ))?)
        } else {
          Ok(serde_wasm_bindgen::to_value(&Update::Multi(MultiUpdate::Mouvement(
            selected, query.selected
          )))?)
        }
      },
      _ => {
        let to_marks = self.get_marks(&query.chessboard, &query.selected);
        if to_marks.is_empty() {
          self.previous_result = None;
          Ok(serde_wasm_bindgen::to_value(&Update::Clear)?)
        } else {
          self.previous_result = Some(PreviousResult {
            selected: query.selected.clone(), result: to_marks.clone()
          });
          Ok(serde_wasm_bindgen::to_value(&Update::Mark(
            query.selected.clone(), to_marks
          ))?)
        }
      },
    }
  }
}
