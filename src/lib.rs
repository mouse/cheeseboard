mod webrtc;
pub mod web;
pub mod worker;

use std::panic;
use std::{cell::RefCell, rc::Rc};

use wasm_bindgen::prelude::*;

use worker::{Update, Query};
use webrtc::start_webrtc;

#[wasm_bindgen]
pub async fn start() {
  panic::set_hook(Box::new(console_error_panic_hook::hook));

  let data_channel: web_sys::RtcDataChannel = start_webrtc().await.unwrap();

  let worker_prolog = web_sys::Worker::new("./web/worker.js").unwrap();
  let worker_handle = Rc::new(RefCell::new(worker_prolog.clone()));

  let window: web_sys::Window = web_sys::window().expect("no global `window` exists");
  let document = window.document().expect("should have a document on window");
  let element_chessboard = document.get_element_by_id("chessboard").unwrap();
  let element_chessboard: &web_sys::HtmlElement =
    element_chessboard.dyn_ref::<web_sys::HtmlElement>().unwrap();

  let query = window.location().search().unwrap();
  let params = web_sys::UrlSearchParams::new_with_str(&query).unwrap();
  if let Some(serialized_state) = params.get(web::PARAM_CHESS) {
    web::MultiUpdate::Start(serialized_state).update();
  }
  if params.get(webrtc::PARAM_CODE).is_some() {
    let player_black = web::Player::Black.to_string();

    element_chessboard.set_attribute("data-player", &player_black).unwrap();
  }

  let document_clone = document.clone();
  let data_channel_clone = data_channel.clone();
  let callback = Closure::wrap(Box::new(move |event: web_sys::MessageEvent| {
    (1..65).for_each(|index| {
      document_clone.get_element_by_id(&index.to_string()).unwrap()
              .remove_attribute("target").unwrap();
      document_clone.get_element_by_id(&index.to_string()).unwrap()
              .remove_attribute("armed").unwrap();
    });
    match serde_wasm_bindgen::from_value(event.data()) {
      Ok(Update::Mark(armed, list)) => {
        document_clone.get_element_by_id(&armed).unwrap()
                      .set_attribute("armed", "armed").unwrap();
        list.iter().for_each(|index|
          document_clone.get_element_by_id(index).unwrap()
                        .set_attribute("target", "target").unwrap())
      },
      Ok(Update::Promote(id_from, id_to, promotions)) => {
        // Unset pawn
        let element_from = document_clone.get_element_by_id(&id_from).unwrap();
        element_from.set_text_content(Some(&"."));

        // Set promotion
        let element_chessboard = document_clone.get_element_by_id("chessboard").unwrap();
        element_chessboard.set_attribute("data-promotion", "promotion").unwrap();

        let element_promotion = web::create_promotion(&document_clone, &promotions);

        let element_cell = document_clone.get_element_by_id(&id_to).unwrap();
        element_cell.set_text_content(None);
        element_cell.append_child(&element_promotion).unwrap();

        let data_channel_clone = data_channel_clone.clone();
        let on_change = Closure::wrap(Box::new(move |event: web_sys::Event| {
          let option_element = event.target().unwrap()
                .unchecked_into::<web_sys::HtmlInputElement>();
          let piece = option_element.value();

          element_chessboard.remove_attribute("data-promotion").unwrap();

          let multi = web::MultiUpdate::Promotion(id_from.clone(), id_to.clone(), piece);
          multi.update();

          let bytes = serde_json::to_string(&multi).unwrap();
          let data_channel_clone = data_channel_clone.clone();
          let _ = data_channel_clone.send_with_str(&bytes);
        }) as Box<dyn FnMut(_)>);

        element_promotion.set_onchange(Some(on_change.as_ref().unchecked_ref()));
        on_change.forget();
      },
      Ok(Update::Multi(multi)) => {
        multi.update();
        let bytes = serde_json::to_string(&multi).unwrap();
        let _ = data_channel_clone.send_with_str(&bytes);
      },
      _ => {},
    }
  }) as Box<dyn FnMut(_)>);

  let document_clone = document.clone();
  let worker_handle_clone = worker_handle.clone();
  let on_mouseup = Closure::wrap(Box::new(move |mouse: web_sys::Event| {
    let mouse_element = mouse.target().unwrap();
    let element = mouse_element.dyn_ref::<web_sys::HtmlElement>().unwrap();
    if (Some(String::from("true")) == element.get_attribute("draggable") ||
        Some(String::from("target")) == element.get_attribute("target")) &&
        web::has_to_play(&document_clone) {
      let selected = element.get_attribute("id").unwrap();
      let query = Query {
        chessboard: web::get_chessboard(&document_clone),
        selected: selected,
      };
      let data_query = serde_wasm_bindgen::to_value(&query).unwrap();
      let ref worker_handle = worker_handle_clone.borrow();

      worker_handle.post_message(&data_query).unwrap();
    }
  }) as Box<dyn FnMut(_)>);

  let document_clone = document.clone();
  let worker_handle_clone = worker_handle.clone();
  let drag_start = Closure::wrap(Box::new(move |drag: web_sys::DragEvent| {
    let drag_element = drag.target().unwrap();
    let element = drag_element.dyn_ref::<web_sys::HtmlElement>().unwrap();
    if Some(String::from("true")) == element.get_attribute("draggable") &&
       web::has_to_play(&document_clone) {
      let selected = element.get_attribute("id").unwrap();
      let query = Query {
        chessboard: web::get_chessboard(&document_clone),
        selected: selected,
      };
      let data_query = serde_wasm_bindgen::to_value(&query).unwrap();
      let ref worker_handle = worker_handle_clone.borrow();

      worker_handle.post_message(&data_query).unwrap();
    }
  }) as Box<dyn FnMut(_)>);

  let document_clone = document.clone();
  let worker_handle_clone = worker_handle.clone();
  let data_channel_clone = data_channel.clone();
  let drag_drop = Closure::wrap(Box::new(move |drop_event: web_sys::DragEvent| {
    drop_event.prevent_default();

    if let Some(file) = drop_event.data_transfer()
        .and_then(|transfer| transfer.files())
        .and_then(|files| files.get(0)) {
      let data_channel_clone = data_channel_clone.clone();
      let file_open_closure = Closure::wrap(Box::new(move |event: JsValue| {
        let uint8array = js_sys::Uint8Array::new(&event);

        let mut slice = vec![0; uint8array.length() as usize];
        uint8array.copy_to(&mut slice[..]);

        let multi = web::MultiUpdate::Piece(slice);
        multi.update();

        let bytes = serde_json::to_string(&multi).unwrap();
        let _ = data_channel_clone.send_with_str(&bytes);
      }) as Box<dyn FnMut(_)>);

      let _promise = file.array_buffer().then(&file_open_closure);

      file_open_closure.forget();
    } else {
      let drop_element = drop_event.target().unwrap();
      let element = drop_element.dyn_ref::<web_sys::HtmlElement>().unwrap();
      if Some(String::from("target")) == element.get_attribute("target") &&
         web::has_to_play(&document_clone) {
        let selected = element.get_attribute("id").unwrap();
        let query = Query {
          chessboard: web::get_chessboard(&document_clone),
          selected: selected,
        };
        let data_query = serde_wasm_bindgen::to_value(&query).unwrap();
        let ref worker_handle = worker_handle_clone.borrow();

        worker_handle.post_message(&data_query).unwrap();
      }
    }
  }) as Box<dyn FnMut(_)>);

  let drag_over = Closure::wrap(Box::new(move |event: web_sys::DragEvent| {
    event.prevent_default();
  }) as Box<dyn FnMut(_)>);

  let window_clone: web_sys::Window = window.clone();
  let data_channel_clone = data_channel.clone();
  let on_popstate = Closure::wrap(Box::new(move |_event: web_sys::Event| {
    let query = window_clone.location().search().unwrap();
    let params = web_sys::UrlSearchParams::new_with_str(&query).unwrap();
    if let Some(serialized_state) = params.get(web::PARAM_CHESS) {
      let multi = web::MultiUpdate::Start(serialized_state.clone());
      multi.update();

      let bytes = serde_json::to_string(&multi).unwrap();
      let _ = data_channel_clone.send_with_str(&bytes);
    }
  }) as Box<dyn FnMut(_)>);

  worker_prolog.set_onmessage(Some(callback.as_ref().unchecked_ref()));
  element_chessboard.set_onmouseup(Some(on_mouseup.as_ref().unchecked_ref()));
  element_chessboard.set_ondragstart(Some(drag_start.as_ref().unchecked_ref()));
  element_chessboard.set_ondragover(Some(drag_over.as_ref().unchecked_ref()));
  window.set_onpopstate(Some(on_popstate.as_ref().unchecked_ref()));
  element_chessboard.set_ondrop(Some(drag_drop.as_ref().unchecked_ref()));
  callback.forget();
  on_mouseup.forget();
  drag_start.forget();
  drag_over.forget();
  drag_drop.forget();
  on_popstate.forget();
}
